use window::run;
use winit::event_loop::EventLoop;

mod window;

fn main() {
    let event_loop = EventLoop::new().unwrap();
    let window = winit::window::Window::new(&event_loop).unwrap();
    window.set_title("Cool fricking game");
    pollster::block_on(window::run(event_loop, window));
}
